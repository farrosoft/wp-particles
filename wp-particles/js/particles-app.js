jQuery(function ($) {
    $(document).ready(function () {
        var particles_ids = $('[data-wrapper-id]');
        $(window).on('ajax_success', function (event, response, particles_id) {
            var quantity = $('[data-id=' + particles_id + ']').length;
            console.log(quantity + ' | ' + particles_id);
            var id = 'particles-' + particles_id + '-' + quantity;
            var content = '<div id = "' + id + '" data-id = "' + particles_id + '"></div>';
            particles_ids.eq(quantity).append(content);
            particlesJS(id, response);
        });
        for (var i = 0; i < particles_ids.length; i++) {
            var particles_id = particles_ids.eq(i).attr('data-wrapper-id');

            $.ajax({
                url: ajax_url.url,
                type: 'post',
                data: {
                    "action": 'particles_data_handler',
                    "data": {
                        "id": particles_id,
                        "meta": "_fs_particles"
                    }
                },
                success: function (response) {
                    var response_object = $.parseJSON(response).meta;
                    var response_object_id = $.parseJSON(response).id;
                    $(window).trigger('ajax_success', [response_object, response_object_id]);
                }
            });

        }

    });
});