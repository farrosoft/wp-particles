<?php

function call_WpParticlesInfoBox()
{
    new WpParticlesInfoBox();
}

if (is_admin()) {
    add_action('load-post.php', 'call_WpParticlesInfoBox');
    add_action('load-post-new.php', 'call_WpParticlesInfoBox');
}


class WpParticlesInfoBox
{

    public function __construct()
    {
        add_action('add_meta_boxes', array($this, 'add_meta_box'));
    }

    public function add_meta_box($post_type)
    {
        $post_types = array('particles-item');
        if (in_array($post_type, $post_types)) {
            add_meta_box(
                'fs_particles_shortcode_meta'
                , __('Particles Shortcode', DOMAIN)
                , array($this, 'render_meta_box_content')
                , $post_type
                , 'advanced'
                , 'high'
            );
        }
    }


    public function render_meta_box_content()
    {
        echo '<div class="shortcode-info-box">
                <span class="dashicons dashicons-warning"></span>
                <span>[particles id = "' . get_the_ID() . '" ]</span>
              </div> ';
    }
}