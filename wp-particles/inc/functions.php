<?php

add_action('init', 'fs_register_post_types');
function fs_register_post_types()
{
    $args = array(
        'labels' => array(
            'name' => 'Particles',
            'singular_name' => 'Particles',
            'menu_name' => 'Particles',
            'add_new' => 'Create New Particles',
            'add_new_item' => 'Particles Settings',
            'edit_item' => 'Edit Particles Settings',
            'new_item' => 'New Particles',
        ),
        'public' => true,
        'hierarchical' => false,
        'supports' => array('title'),
        'taxonomies' => array(),
        'has_archive' => false,
        'menu_position' => 99,
        'menu_icon' => 'dashicons-image-filter',
        'rewrite' => array('slug' => 'particles-item'),
    );
    register_post_type('particles-item', $args);
}

add_action('wp_enqueue_scripts', 'register_particles_scripts');
function register_particles_scripts()
{
    wp_enqueue_script('wp-particles', plugins_url('/wp-particles/js/particles.min.js'), '', '', true);
    wp_enqueue_script('wp-particles-app', plugins_url('/wp-particles/js/particles-app.js'), '', '', true);
}

//Enqueue Admin Scripts And Styles

add_action('admin_enqueue_scripts', 'enqueue_admin_styles');
function enqueue_admin_styles()
{
    wp_enqueue_media();
    wp_enqueue_style('wp-color-picker');
    wp_enqueue_style('admin-css', plugins_url('/wp-particles/admin/admin.css'));
    wp_enqueue_script('admin-js', plugins_url('/wp-particles/admin/admin.js'), array('wp-color-picker'), false, true);
}

//Add Filter To Display Shortcode With Widget Text

add_filter('widget_text', 'do_shortcode');

require_once(plugin_dir_path(__FILE__) . 'class-particles-info-box.php');
require_once(plugin_dir_path(__FILE__) . 'class-particles-metabox.php');
require_once(plugin_dir_path(__FILE__) . 'class-particles-shortcode.php');



