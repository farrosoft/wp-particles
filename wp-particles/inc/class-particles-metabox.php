<?php
function call_WpParticles()
{
    new WpParticles();
}

if (is_admin()) {
    add_action('load-post.php', 'call_WpParticles');
    add_action('load-post-new.php', 'call_WpParticles');
}

class WpParticles
{

    public function __construct()
    {
        add_action('add_meta_boxes', array($this, 'add_meta_box'));
        add_action('save_post', array($this, 'save'));
    }

    public function add_meta_box($post_type)
    {

        $post_types = array('particles-item');
        if (in_array($post_type, $post_types)) {
            add_meta_box(
                'fs_particles'
                , __('Particles Settings', DOMAIN)
                , array($this, 'render_meta_box_content')
                , $post_type
                , 'advanced'
                , 'high'
            );
        }
    }


    public function save($post_id)
    {

        if (!isset($_POST['fs_inner_custom_box_nonce']))
            return $post_id;

        $nonce = $_POST['fs_inner_custom_box_nonce'];

        if (!wp_verify_nonce($nonce, 'fs_inner_custom_box'))
            return $post_id;

        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return $post_id;

        if ('page' == $_POST['post_type']) {

            if (!current_user_can('edit_page', $post_id))
                return $post_id;

        } else {

            if (!current_user_can('edit_post', $post_id))
                return $post_id;
        }

        if (isset($_POST['particles_number_value'])) {
            $particles_number_value = isset($_POST['particles_number_value']) ? $_POST['particles_number_value'] : 100;
            $particles_number_density_enable = isset($_POST['particles_number_density_enable']) ? $this->check_logic_instance($_POST['particles_number_density_enable']) : true;
            $particles_number_density_value_area = isset($_POST['particles_number_density_value_area']) ? $_POST['particles_number_density_value_area'] : 1500;
            $particles_color_value = isset($_POST['particles_color_value']) ? $_POST['particles_color_value'] : array('#0075ba');
            $particles_shape_type = isset($_POST['particles_shape_type']) ? $_POST['particles_shape_type'] : array('circle');
            $particles_shape_stroke_width = isset($_POST['particles_shape_stroke_width']) ? $_POST['particles_shape_stroke_width'] : 0;
            $particles_shape_stroke_color = isset($_POST['particles_shape_stroke_color']) ? $_POST['particles_shape_stroke_color'] : "#000000";
            $particles_shape_polygon_nb_sides = isset($_POST['particles_shape_polygon_nb_sides']) ? $_POST['particles_shape_polygon_nb_sides'] : 5;
            $particles_shape_image_src = isset($_POST['particles_shape_image_src']) ? $_POST['particles_shape_image_src'] : plugins_url('/wp-particles/img/wordpress-logo.png');
            $particles_shape_image_width = isset($_POST['particles_shape_image_width']) ? $_POST['particles_shape_image_width'] : 100;
            $particles_shape_image_height = isset($_POST['particles_shape_image_height']) ? $_POST['particles_shape_image_height'] : 100;
            $particles_opacity_value = isset($_POST['particles_opacity_value']) ? $_POST['particles_opacity_value'] : 1;
            $particles_opacity_random = isset($_POST['particles_opacity_random']) ? $this->check_logic_instance($_POST['particles_opacity_random']) : false;
            $particles_opacity_anim_enable = isset($_POST['particles_opacity_anim_enable']) ? $this->check_logic_instance($_POST['particles_opacity_anim_enable']) : false;
            $particles_opacity_anim_speed = isset($_POST['particles_opacity_anim_speed']) ? $_POST['particles_opacity_anim_speed'] : 0;
            $particles_opacity_anim_opacity_min = isset($_POST['particles_opacity_anim_opacity_min']) ? $_POST['particles_opacity_anim_opacity_min'] : 0;
            $particles_opacity_anim_sync = isset($_POST['particles_opacity_anim_sync']) ? $this->check_logic_instance($_POST['particles_opacity_anim_sync']) : false;
            $particles_size_value = isset($_POST['particles_size_value']) ? $_POST['particles_size_value'] : 3;
            $particles_size_random = isset($_POST['particles_size_random']) ? $this->check_logic_instance($_POST['particles_size_random']) : true;
            $particles_size_anim_enable = isset($_POST['particles_size_anim_enable']) ? $this->check_logic_instance($_POST['particles_size_anim_enable']) : false;
            $particles_size_anim_speed = isset($_POST['particles_size_anim_speed']) ? $_POST['particles_size_anim_speed'] : 0;
            $particles_size_anim_size_min = isset($_POST['particles_size_anim_size_min']) ? $_POST['particles_size_anim_size_min'] : 4;
            $particles_size_anim_sync = isset($_POST['particles_size_anim_sync']) ? $this->check_logic_instance($_POST['particles_size_anim_sync']) : false;
            $particles_line_linked_enable = isset($_POST['particles_line_linked_enable']) ? $this->check_logic_instance($_POST['particles_line_linked_enable']) : true;
            $particles_line_linked_distance = isset($_POST['particles_line_linked_distance']) ? $_POST['particles_line_linked_distance'] : 200;
            $particles_line_linked_color = isset($_POST['particles_line_linked_color']) ? $_POST['particles_line_linked_color'] : "#0075ba";
            $particles_line_linked_opacity = isset($_POST['particles_line_linked_opacity']) ? $_POST['particles_line_linked_opacity'] : 0.5;
            $particles_line_linked_width = isset($_POST['particles_line_linked_width']) ? $_POST['particles_line_linked_width'] : 1;
            $particles_move_enable = isset($_POST['particles_move_enable']) ? $this->check_logic_instance($_POST['particles_move_enable']) : true;
            $particles_move_speed = isset($_POST['particles_move_speed']) ? $_POST['particles_move_speed'] : 2;
            $particles_move_direction = isset($_POST['particles_move_direction']) ? $_POST['particles_move_direction'] : "none";
            $particles_move_random = isset($_POST['particles_move_random']) ? $this->check_logic_instance($_POST['particles_move_random']) : false;
            $particles_move_straight = isset($_POST['particles_move_straight']) ? $this->check_logic_instance($_POST['particles_move_straight']) : false;
            $particles_move_out_mode = isset($_POST['particles_move_out_mode']) ? $_POST['particles_move_out_mode'] : "out";
            $particles_move_bounce = isset($_POST['particles_move_bounce']) ? $this->check_logic_instance($_POST['particles_move_bounce']) : false;
            $particles_move_attract_enable = isset($_POST['particles_move_attract_enable']) ? $this->check_logic_instance($_POST['particles_move_attract_enable']) : false;
            $particles_move_attract_rotateX = isset($_POST['particles_move_attract_rotateX']) ? $_POST['particles_move_attract_rotateX'] : 600;
            $particles_move_attract_rotateY = isset($_POST['particles_move_attract_rotateY']) ? $_POST['particles_move_attract_rotateY'] : 600;

            $interactivity_detect_on = isset($_POST['interactivity_detect_on']) ? $_POST['interactivity_detect_on'] : "canvas";
            $interactivity_events_onhover_enable = isset($_POST['interactivity_events_onhover_enable']) ? $this->check_logic_instance($_POST['interactivity_events_onhover_enable']) : true;
            $interactivity_events_onhover_mode = isset($_POST['interactivity_events_onhover_mode']) ? $_POST['interactivity_events_onhover_mode'] : 'grab';
            $interactivity_events_onclick_enable = isset($_POST['interactivity_events_onclick_enable']) ? $this->check_logic_instance($_POST['interactivity_events_onclick_enable']) : true;
            $interactivity_events_onclick_mode = isset($_POST['interactivity_events_onclick_mode']) ? $_POST['interactivity_events_onclick_mode'] : 'push';
            $interactivity_events_resize = isset($_POST['interactivity_events_resize']) ? $this->check_logic_instance($_POST['interactivity_events_resize']) : false;
            $interactivity_modes_grab_distance = isset($_POST['interactivity_modes_grab_distance']) ? $_POST['interactivity_modes_grab_distance'] : 200;
            $interactivity_modes_grab_line_linked_opacity = isset($_POST['interactivity_modes_grab_line_linked_opacity']) ? $_POST['interactivity_modes_grab_line_linked_opacity'] : 0.8;
            $interactivity_modes_bubble_distance = isset($_POST['interactivity_modes_bubble_distance']) ? $_POST['interactivity_modes_bubble_distance'] : 800;
            $interactivity_modes_bubble_size = isset($_POST['interactivity_modes_bubble_size']) ? $_POST['interactivity_modes_bubble_size'] : 80;
            $interactivity_modes_bubble_duration = isset($_POST['interactivity_modes_bubble_duration']) ? $_POST['interactivity_modes_bubble_duration'] : 2;
            $interactivity_modes_bubble_opacity = isset($_POST['interactivity_modes_bubble_opacity']) ? $_POST['interactivity_modes_bubble_opacity'] : 0.8;
            $interactivity_modes_bubble_speed = isset($_POST['interactivity_modes_bubble_speed']) ? $_POST['interactivity_modes_bubble_speed'] : 3;
            $interactivity_modes_repulse_distance = isset($_POST['interactivity_modes_repulse_distance']) ? $_POST['interactivity_modes_repulse_distance'] : 200;
            $interactivity_modes_repulse_duration = isset($_POST['interactivity_modes_repulse_duration']) ? $_POST['interactivity_modes_repulse_duration'] : 0.4;
            $interactivity_modes_push_particles_nb = isset($_POST['interactivity_modes_push_particles_nb']) ? $_POST['interactivity_modes_push_particles_nb'] : 4;
            $interactivity_modes_remove_particles_nb = isset($_POST['interactivity_modes_remove_particles_nb']) ? $_POST['interactivity_modes_remove_particles_nb'] : 2;

            $data = array(
                "particles" => [
                    "number" => [
                        "value" => $particles_number_value,
                        "density" => [
                            "enable" => $particles_number_density_enable,
                            "value_area" => $particles_number_density_value_area
                        ]
                    ],
                    "color" => [
                        "value" => $particles_color_value
                    ],
                    "shape" => [
                        "type" => $particles_shape_type,
                        "stroke" => [
                            "width" => $particles_shape_stroke_width,
                            "color" => $particles_shape_stroke_color
                        ],
                        "polygon" => [
                            "nb_sides" => $particles_shape_polygon_nb_sides
                        ],
                        "image" => [
                            "src" => $particles_shape_image_src,
                            "width" => $particles_shape_image_width,
                            "height" => $particles_shape_image_height
                        ]
                    ],
                    "opacity" => [
                        "value" => $particles_opacity_value,
                        "random" => $particles_opacity_random,
                        "anim" => [
                            "enable" => $particles_opacity_anim_enable,
                            "speed" => $particles_opacity_anim_speed,
                            "opacity_min" => $particles_opacity_anim_opacity_min,
                            "sync" => $particles_opacity_anim_sync
                        ]
                    ],
                    "size" => [
                        "value" => $particles_size_value,
                        "random" => $particles_size_random,
                        "anim" => [
                            "enable" => $particles_size_anim_enable,
                            "speed" => $particles_size_anim_speed,
                            "size_min" => $particles_size_anim_size_min,
                            "sync" => $particles_size_anim_sync
                        ]
                    ],
                    "line_linked" => [
                        "enable" => $particles_line_linked_enable,
                        "distance" => $particles_line_linked_distance,
                        "color" => $particles_line_linked_color,
                        "opacity" => $particles_line_linked_opacity,
                        "width" => $particles_line_linked_width
                    ],
                    "move" => [
                        "enable" => $particles_move_enable,
                        "speed" => $particles_move_speed,
                        "direction" => $particles_move_direction,
                        "random" => $particles_move_random,
                        "straight" => $particles_move_straight,
                        "out_mode" => $particles_move_out_mode,
                        "bounce" => $particles_move_bounce,
                        "attract" => [
                            "enable" => $particles_move_attract_enable,
                            "rotateX" => $particles_move_attract_rotateX,
                            "rotateY" => $particles_move_attract_rotateY
                        ]
                    ]
                ],
                "interactivity" => [
                    "detect_on" => $interactivity_detect_on,
                    "events" => [
                        "onhover" => [
                            "enable" => $interactivity_events_onhover_enable,
                            "mode" => $interactivity_events_onhover_mode
                        ],
                        "onclick" => [
                            "enable" => $interactivity_events_onclick_enable,
                            "mode" => $interactivity_events_onclick_mode
                        ],
                        "resize" => $interactivity_events_resize
                    ],
                    "modes" => [
                        "grab" => [
                            "distance" => $interactivity_modes_grab_distance,
                            "line_linked" => [
                                "opacity" => $interactivity_modes_grab_line_linked_opacity
                            ]
                        ],
                        "bubble" => [
                            "distance" => $interactivity_modes_bubble_distance,
                            "size" => $interactivity_modes_bubble_size,
                            "duration" => $interactivity_modes_bubble_duration,
                            "opacity" => $interactivity_modes_bubble_opacity,
                            "speed" => $interactivity_modes_bubble_speed
                        ],
                        "repulse" => [
                            "distance" => $interactivity_modes_repulse_distance,
                            "duration" => $interactivity_modes_repulse_duration
                        ],
                        "push" => [
                            "particles_nb" => $interactivity_modes_push_particles_nb
                        ],
                        "remove" => [
                            "particles_nb" => $interactivity_modes_remove_particles_nb
                        ]
                    ]
                ],
                "retina_detect" => true
            );

            update_post_meta($post_id, '_fs_particles', $data);
        }
    }


    public function render_meta_box_content($post)
    {

        wp_nonce_field('fs_inner_custom_box', 'fs_inner_custom_box_nonce');

        $meta = get_post_meta($post->ID, '_fs_particles', true);
        $default = array(
            "particles" => [
                "number" => [
                    "value" => 100,
                    "density" => [
                        "enable" => true,
                        "value_area" => 1500
                    ]
                ],
                "color" => [
                    "value" => "#0075ba"
                ],
                "shape" => [
                    "type" => array('circle'),
                    "stroke" => [
                        "width" => 0,
                        "color" => "#000000"
                    ],
                    "polygon" => [
                        "nb_sides" => 5
                    ],
                    "image" => [
                        "src" => plugins_url('/wp-particles/img/wordpress-logo.png'),
                        "width" => 100,
                        "height" => 100
                    ]
                ],
                "opacity" => [
                    "value" => 1,
                    "random" => false,
                    "anim" => [
                        "enable" => false,
                        "speed" => 0,
                        "opacity_min" => 0,
                        "sync" => false
                    ]
                ],
                "size" => [
                    "value" => 3,
                    "random" => true,
                    "anim" => [
                        "enable" => false,
                        "speed" => 0,
                        "size_min" => 4,
                        "sync" => false
                    ]
                ],
                "line_linked" => [
                    "enable" => true,
                    "distance" => 200,
                    "color" => "#0075ba",
                    "opacity" => 0.5,
                    "width" => 1
                ],
                "move" => [
                    "enable" => true,
                    "speed" => 2,
                    "direction" => "none",
                    "random" => false,
                    "straight" => false,
                    "out_mode" => "out",
                    "bounce" => false,
                    "attract" => [
                        "enable" => false,
                        "rotateX" => 600,
                        "rotateY" => 600
                    ]
                ]
            ],
            "interactivity" => [
                "detect_on" => "canvas",
                "events" => [
                    "onhover" => [
                        "enable" => true,
                        "mode" => "grab"
                    ],
                    "onclick" => [
                        "enable" => true,
                        "mode" => "push"
                    ],
                    "resize" => false
                ],
                "modes" => [
                    "grab" => [
                        "distance" => 200,
                        "line_linked" => [
                            "opacity" => 0.8
                        ]
                    ],
                    "bubble" => [
                        "distance" => 800,
                        "size" => 80,
                        "duration" => 2,
                        "opacity" => 1,
                        "speed" => 3
                    ],
                    "repulse" => [
                        "distance" => 200,
                        "duration" => 0.4
                    ],
                    "push" => [
                        "particles_nb" => 4
                    ],
                    "remove" => [
                        "particles_nb" => 2
                    ]
                ]
            ],
            "retina_detect" => true
        );
        $particles = isset($meta) && !empty($meta) ? get_post_meta($post->ID, '_fs_particles', true) : $default;
        ?>
        <div class="particles fs-row">
            <span class="fs-title">Particles</span>
            <div class="number fs-block">
                <span class="fs-title">number</span>
                <div class="value fs-item-inner">
                    <?php $this->add_range_control( 'particles_number_value', $particles['particles']['number']['value'], 0, 600, 1 ); ?>
                </div>
                <div class="density fs-block">
                    <span class="fs-title">density</span>
                    <div class="enable fs-item-inner">
                        <?php $this->add_logic_control('particles_number_density_enable', $particles['particles']['number']['density']['enable']); ?>
                    </div>
                    <div class="value_area fs-item-inner">
                        <?php $this->add_range_control( 'particles_number_density_value_area', $particles['particles']['number']['density']['value_area'], 100, 10000, 1 ); ?>
                    </div>
                </div>
            </div>
            <div class="color fs-block">
                <span class="fs-title">color</span>
                <div class="value fs-item-inner">
                    <?php $this->add_color_control('particles_color_value', $particles['particles']['color']['value']); ?>
                </div>
            </div>
            <div class="shape fs-block">
                <span class="fs-title">shape</span>
                <div class="type fs-item-inner">
                    <?php
                    $shape_types = array("circle", "edge", "triangle", "polygon", "star");
                    $this->add_checkbox_control('particles_shape_type', $particles['particles']['shape']['type'], $shape_types);
                    ?>
                </div>
                <div class="stroke fs-block">
                    <span class="fs-title">stroke</span>
                    <div class="width fs-item-inner">
                        <?php $this->add_range_control( 'particles_shape_stroke_width', $particles['particles']['shape']['stroke']['width'], 0, 20, 0.1 ); ?>
                    </div>
                    <div class="color fs-item-inner">
                        <?php $this->add_color_control('particles_shape_stroke_color', $particles['particles']['shape']['stroke']['color']); ?>
                    </div>
                </div>
                <div class="polygon fs-block">
                    <span class="fs-title">polygon</span>
                    <div class="nb_sides fs-item-inner">
                        <?php $this->add_range_control( 'particles_shape_polygon_nb_sides', $particles['particles']['shape']['polygon']['nb_sides'], 3, 12, 1 ); ?>
                    </div>
                </div>
                <div class="image fs-block">
                    <span class="fs-title">image</span>
                    <div class="src fs-item-inner">
                        <label
                            for="particles_shape_image_src"><?php _e('particles_shape_image_src', DOMAIN); ?></label><br/>
                        <?php
                        if (!empty($particles['particles']['shape']['image']['src'])) :
                            echo '<img class="custom_media_image" src="' . $particles['particles']['shape']['image']['src'] . '" /><br />';
                        endif;
                        ?>
                        <input class="text custom_media_url" type="text" id="particles_shape_image_src"
                               name="particles_shape_image_src"
                               value="<?php echo $particles['particles']['shape']['image']['src']; ?>"/>
                        <input type="button" class="button button-primary custom_media_button" id="custom_media_button"
                               name="particles_shape_image_src" value="<?php _e('Upload Image', DOMAIN); ?>"/>
                    </div>
                    <div class="width fs-item-inner">
                        <?php $this->add_range_control( 'particles_shape_image_width', $particles['particles']['shape']['image']['width'], 0, 10000, 1 ); ?>
                    </div>
                    <div class="height fs-item-inner">
                        <?php $this->add_range_control( 'particles_shape_image_height', $particles['particles']['shape']['image']['height'], 0, 10000, 1 ); ?>
                    </div>
                </div>
            </div>
            <div class="opacity fs-block">
                <span class="fs-title">opacity</span>
                <div class="value fs-item-inner">
                    <?php $this->add_range_control( 'particles_opacity_value', $particles['particles']['opacity']['value'], 0, 1, 0.1 ); ?>
                </div>
                <div class="random fs-item-inner">
                    <?php $this->add_logic_control('particles_opacity_random', $particles['particles']['opacity']['random']) ?>
                </div>
                <div class="anim fs-block">
                    <span class="fs-title">anim</span>
                    <div class="enable fs-item-inner">
                        <?php $this->add_logic_control('particles_opacity_anim_enable', $particles['particles']['opacity']['anim']['enable']) ?>
                    </div>
                    <div class="speed fs-item-inner">
                        <?php $this->add_range_control( 'particles_opacity_anim_speed', $particles['particles']['opacity']['anim']['speed'], 0, 10, 0.1 ); ?>
                    </div>
                    <div class="opacity_min fs-item-inner">
                        <?php $this->add_range_control( 'particles_opacity_anim_opacity_min', $particles['particles']['opacity']['anim']['opacity_min'], 0, 1, 0.1 ); ?>
                    </div>
                    <div class="sync fs-item-inner">
                        <?php $this->add_logic_control('particles_opacity_anim_sync', $particles['particles']['opacity']['anim']['sync']) ?>
                    </div>
                </div>
            </div>
            <div class="size fs-block">
                <span class="fs-title">size</span>
                <div class="value fs-item-inner">
                    <?php $this->add_range_control( 'particles_size_value', $particles['particles']['size']['value'], 0, 500, 0.1 ); ?>
                </div>
                <div class="random fs-item-inner">
                    <?php $this->add_logic_control('particles_size_random', $particles['particles']['size']['random']) ?>
                </div>
                <div class="anim fs-block">
                    <span class="fs-title">anim</span>
                    <div class="enable fs-item-inner">
                        <?php $this->add_logic_control('particles_size_anim_enable', $particles['particles']['size']['anim']['enable']) ?>
                    </div>
                    <div class="speed fs-item-inner">
                        <?php $this->add_range_control( 'particles_size_anim_speed', $particles['particles']['size']['anim']['speed'], 0, 300, 1 ); ?>
                    </div>
                    <div class="size_min fs-item-inner">
                        <?php $this->add_range_control( 'particles_size_anim_size_min', $particles['particles']['size']['anim']['size_min'], 0, 100, 1 ); ?>
                    </div>
                    <div class="sync fs-item-inner">
                        <?php $this->add_logic_control('particles_size_anim_sync', $particles['particles']['size']['anim']['sync']) ?>
                    </div>
                </div>
            </div>
            <div class="line_linked fs-block">
                <span class="fs-title">line_linked</span>
                <div class="enable fs-item-inner">
                    <?php $this->add_logic_control('particles_line_linked_enable', $particles['particles']['line_linked']['enable']) ?>
                </div>
                <div class="distance fs-item-inner">
                    <?php $this->add_range_control( 'particles_line_linked_distance', $particles['particles']['line_linked']['distance'], 0, 2000, 1 ); ?>
                </div>
                <div class="color fs-item-inner">
                    <?php $this->add_color_control('particles_line_linked_color', $particles['particles']['line_linked']['color']); ?>
                </div>
                <div class="opacity fs-item-inner">
                    <?php $this->add_range_control( 'particles_line_linked_opacity', $particles['particles']['line_linked']['opacity'], 0, 1, 0.1 ); ?>
                </div>
                <div class="width fs-item-inner">
                    <?php $this->add_range_control( 'particles_line_linked_width', $particles['particles']['line_linked']['width'], 0, 20, 0.1 ); ?>
                </div>
            </div>
            <div class="move fs-block">
                <span class="fs-title">move</span>
                <div class="enable fs-item-inner">
                    <?php $this->add_logic_control('particles_move_enable', $particles['particles']['move']['enable']) ?>
                </div>
                <div class="speed fs-item-inner">
                    <?php $this->add_range_control( 'particles_move_speed', $particles['particles']['move']['speed'], 0, 200, 1 ); ?>
                </div>
                <div class="direction fs-item-inner">
                    <?php
                    $directions = array("none", "top", "top-right", "right", "bottom-right", "bottom", "bottom-left", "left", "top-left");
                    $this->add_radio_control('particles_move_direction', $particles['particles']['move']['direction'], $directions);
                    ?>
                </div>
                <div class="random fs-item-inner">
                    <?php $this->add_logic_control('particles_move_random', $particles['particles']['move']['random']) ?>
                </div>
                <div class="straight fs-item-inner">
                    <?php $this->add_logic_control('particles_move_straight', $particles['particles']['move']['straight']) ?>
                </div>
                <div class="out_mode fs-item-inner">
                    <?php
                    $modes = array("out", "bounce");
                    $this->add_radio_control('particles_move_out_mode', $particles['particles']['move']['out_mode'], $modes);
                    ?>
                </div>
                <div class="bounce fs-item-inner">
                    <?php $this->add_logic_control('particles_move_bounce', $particles['particles']['move']['bounce']) ?>
                </div>
                <div class="attract fs-block">
                    <span class="fs-title">attract</span>
                    <div class="enable fs-item-inner">
                        <?php $this->add_logic_control('particles_move_attract_enable', $particles['particles']['move']['attract']['enable']) ?>
                    </div>
                    <div class="rotateX fs-item-inner">
                        <?php $this->add_range_control( 'particles_move_attract_rotateX', $particles['particles']['move']['attract']['rotateX'], 0, 10000, 10 ); ?>
                    </div>
                    <div class="rotateY fs-item-inner">
                        <?php $this->add_range_control( 'particles_move_attract_rotateY', $particles['particles']['move']['attract']['rotateY'], 0, 10000, 10 ); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="interactivity fs-row">
            <span class="fs-title">Iinteractivity</span>
            <div class="detect_on fs-item-inner">
                <?php
                $modes = array("canvas", "window");
                $this->add_radio_control('interactivity_detect_on', $particles['interactivity']['detect_on'], $modes);
                ?>
            </div>
            <div class="events fs-block">
                <span class="fs-title">events</span>
                <div class="onhover fs-block">
                    <span class="fs-title">onhover</span>
                    <div class="enable fs-item-inner">
                        <?php $this->add_logic_control('interactivity_events_onhover_enable', $particles['interactivity']['events']['onhover']['enable']) ?>
                    </div>
                    <div class="mode fs-item-inner">
                        <?php
                        $onhover_modes = array("grab", "bubble", "repulse");
                        $this->add_radio_control('interactivity_events_onhover_mode', $particles['interactivity']['events']['onhover']['mode'], $onhover_modes);
                        ?>
                    </div>
                </div>
                <div class="onclick fs-block">
                    <span class="fs-title">onclick</span>
                    <div class="enable fs-item-inner">
                        <?php $this->add_logic_control('interactivity_events_onclick_enable', $particles['interactivity']['events']['onclick']['enable']) ?>
                    </div>
                    <div class="mode fs-item-inner">
                        <?php
                        $onclick_modes = array("push", "remove", "bubble", "repulse");
                        $this->add_radio_control('interactivity_events_onclick_mode', $particles['interactivity']['events']['onclick']['mode'], $onclick_modes);
                        ?>
                    </div>
                </div>
                <div class="resize fs-item-inner">
                    <?php $this->add_logic_control('interactivity_events_resize', $particles['interactivity']['events']['resize']) ?>
                </div>
            </div>
            <div class="modes fs-block">
                <span class="fs-title">modes</span>
                <div class="grab fs-block">
                    <span class="fs-title">grab</span>
                    <div class="distance fs-item-inner">
                        <?php $this->add_range_control( 'interactivity_modes_grab_distance', $particles['interactivity']['modes']['grab']['distance'], 0, 1500, 1 ); ?>
                    </div>
                    <div class="line_linked fs-block">
                        <span class="fs-title">line linked</span>
                        <div class="opacity fs-item-inner">
                            <?php $this->add_range_control( 'interactivity_modes_grab_line_linked_opacity', $particles['interactivity']['modes']['grab']['line_linked']['opacity'], 0, 1, 0.1 ); ?>
                        </div>
                    </div>
                </div>
                <div class="bubble fs-block">
                    <span class="fs-title">bubble</span>
                    <div class="distance fs-item-inner">
                        <?php $this->add_range_control( 'interactivity_modes_bubble_distance', $particles['interactivity']['modes']['bubble']['distance'], 0, 1500, 1 ); ?>
                    </div>
                    <div class="size fs-item-inner">
                        <?php $this->add_range_control( 'interactivity_modes_bubble_size', $particles['interactivity']['modes']['bubble']['size'], 0, 500, 1 ); ?>
                    </div>
                    <div class="duration fs-item-inner">
                        <?php $this->add_range_control( 'interactivity_modes_bubble_duration', $particles['interactivity']['modes']['bubble']['duration'], 0, 10, 0.1 ); ?>
                    </div>
                    <div class="opacity fs-item-inner">
                        <?php $this->add_range_control( 'interactivity_modes_bubble_opacity', $particles['interactivity']['modes']['bubble']['opacity'], 0, 1, 0.1 ); ?>
                    </div>
                    <div class="speed fs-item-inner">
                        <?php $this->add_range_control( 'interactivity_modes_bubble_speed', $particles['interactivity']['modes']['bubble']['speed'], 0, 500, 1 ); ?>
                    </div>
                </div>
                <div class="repulse fs-block">
                    <span class="fs-title">repulse</span>
                    <div class="distance fs-item-inner">
                        <?php $this->add_range_control( 'interactivity_modes_repulse_distance', $particles['interactivity']['modes']['repulse']['distance'], 0, 1000, 1 ); ?>
                    </div>
                    <div class="duration fs-item-inner">
                        <?php $this->add_range_control( 'interactivity_modes_repulse_duration', $particles['interactivity']['modes']['repulse']['duration'], 0, 10, 0.1 ); ?>
                    </div>
                </div>
                <div class="push fs-block">
                    <span class="fs-title">push</span>
                    <div class="particles_nb fs-item-inner">
                        <?php $this->add_range_control( 'interactivity_modes_push_particles_nb', $particles['interactivity']['modes']['push']['particles_nb'], 0, 10, 1 ); ?>
                    </div>
                </div>
                <div class="remove fs-block">
                    <span class="fs-title">remove</span>
                    <div class="particles_nb fs-item-inner">
                        <?php $this->add_range_control( 'interactivity_modes_remove_particles_nb', $particles['interactivity']['modes']['remove']['particles_nb'], 0, 10, 1 ); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }

    public function check_logic_instance($var)
    {
        return $var == 'true';
    }

    public function add_logic_control($name, $value)
    {
        ?>
        <label for="<?php echo $name; ?>"><?php _e($name, DOMAIN); ?></label><br/>
        <?php
        $directions = array('true', 'false');
        foreach ($directions as $i => $direction) {
            $val = $value ? 'true' : 'false';
            $attr = $direction == $val ? 'checked' : '';
            ?>
            <input type="radio" id="<?php echo $name; ?><?php echo $i; ?>"
                   name="<?php echo $name; ?>"
                   value="<?php echo $direction; ?>" <?php echo $attr; ?>>
            <label class="button button-default"
                   for="<?php echo $name; ?><?php echo $i; ?>"><?php echo $direction; ?></label>
            <?php
        }
        ?>
        <?php
    }
    
    public function add_range_control ($name, $value, $min, $max, $step) {
        ?>
        <label
            for="<?php echo $name; ?>"><?php _e( $name , DOMAIN); ?></label><br/>
        <input class="range" type="range" min="<?php echo $min; ?>" max="<?php echo $max; ?>" step="<?php echo $step; ?>"
               id="<?php echo $name; ?>"
               name="<?php echo $name; ?>"
               value="<?php echo $value; ?>"/>
        <span class="value-container"></span>
        <?php
    }

    public function add_color_control ($name, $value) {
        ?>
        <label for="<?php echo $name; ?>"><?php _e( $name, DOMAIN ); ?></label><br/>
        <input class="colorpicker" id="<?php echo $name; ?>" name="<?php echo $name; ?>"
               value="<?php echo $value; ?>"/>
        <?php
    }

    public function add_radio_control($name, $value, $instances) {
        ?>
        <label
            for="<?php echo $name; ?>"><?php _e( $name, DOMAIN ); ?></label><br/>
        <?php
        foreach ($instances as $i => $instance) {
            $attr = $instance == $value ? 'checked' : '';
            ?>
            <input type="radio" id="<?php echo $name; ?>_<?php echo $i; ?>"
                   name="<?php echo $name; ?>"
                   value="<?php echo $instance; ?>" <?php echo $attr; ?>>
            <label class="button button-default"
                   for="<?php echo $name; ?>_<?php echo $i; ?>"><?php echo $instance ?></label>
            <?php
        }
    }

    public function add_checkbox_control($name, $value, $instances) {
        ?>
        <label
            for="<?php echo $name; ?>"><?php _e( $name, DOMAIN ); ?></label><br/>
        <?php
        foreach ($instances as $i => $instance) {
            $attr = in_array($instance, $value) ? 'checked' : '';
            ?>
            <input type="checkbox" id="<?php echo $name; ?>_<?php echo $i; ?>"
                   name="<?php echo $name; ?>[]"
                   value="<?php echo $instance; ?>" <?php echo $attr; ?>>
            <label class="button button-default"
                   for="<?php echo $name; ?>_<?php echo $i; ?>"><?php echo $instance ?></label>
            <?php
        }
    }

}