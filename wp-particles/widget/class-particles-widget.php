<?php

class ParticlesWidget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(
            'ParticlesWidget',
            __('Particles Widget', DOMAIN)
        );
    }

    function widget($args, $instance)
    {

        extract($args);

        echo $before_widget;

        ?>

        <div data-wrapper-id="<?php echo $instance['particles-item'] ?>"></div>
        <?php

        echo $after_widget;


    }

    function update($new_instance, $old_instance)
    {

        $instance = $old_instance;

        $instance['particles-item'] = ($new_instance['particles-item']);

        return $instance;

    }

    function form($instance)
    {

        ?>

        <p>
            <label
                for="<?php echo $this->get_field_id('particles-item'); ?>"><?php _e('Display Particles', DOMAIN); ?></label><br/>
            <select id="<?php echo $this->get_field_id('particles-item'); ?>"
                    name="<?php echo $this->get_field_name('particles-item'); ?>" class="widefat">
                <?php
                global $post;

                $args = array(
                    'post_status' => 'publish',
                    'post_type' => 'particles-item',
                    'posts_per_page' => -1,
                    'orderby' => 'date',
                    'order' => 'ASC',
                );
                $posts = get_posts($args);
                $i = 0;
                foreach ($posts as $post) {
                    setup_postdata($post);
                    $attr = $instance['particles-item'] == $post->ID ? 'selected' : '';
                    ?>
                    <option value="<?php the_ID(); ?>" <?php echo $attr; ?>><?php the_title(); ?></option>
                    <?php
                    $i++;
                }
                wp_reset_postdata();
                ?>
            </select>
        </p>

        <?php

    }

}

// TODO: Remember to change 'Widget_Name' to match the class name definition
add_action('widgets_init', create_function('', 'register_widget("ParticlesWidget");'));
