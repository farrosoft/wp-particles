jQuery(function ($) {
    $(document).ready(function () {

        //Init ColorPicker
        $('.colorpicker').wpColorPicker();

        //Customize Checkbox
        var checkbox = $('input[type="checkbox"]');
        $.each(checkbox, function (el) {
            if ($(this).prop('checked')) {
                $(this).next('label').addClass('button-primary');
            }
        });

        $(checkbox).click(function () {
            if ($(this).prop('checked')) {
                $(this).next('label').addClass('button-primary');
            } else {
                $(this).next('label').removeClass('button-primary');
            }
        });

        //Customize Radiobatton
        var radio = $('input[type="radio"]');
        $.each(radio, function (el) {
            if ($(this).prop('checked')) {
                $(this).next('label').addClass('button-primary');
            }
        });

        $(radio).click(function () {
            var parent = $(this).parent('div');
            var elements = $(parent).find(radio);
            if ($(this).prop('checked')) {
                $(elements).next('label').removeClass('button-primary');
                $(this).next('label').addClass('button-primary');
            }
        });

        //Return Range Value
        var range = $('input[type="range"]');
        $.each(range, function () {
            $(this).next('span').text($(this).val());
        });
        $(range).on('input change', function () {
            $.each(range, function () {
                $(this).next('span').text($(this).val());
            });
        });

        //Create Dropdown Menu
        var block = $('.fs-block');
        $('.fs-title').click(function () {
            $(this).toggleClass('fs-open');
            $(this).siblings('.fs-block,.fs-item-inner').toggle(300);
        });


        //Add Shortcode Uploder Button
        var shortcode_button = $('#particles-media-button');

        $(shortcode_button).click(open_media_window);

        function open_media_window() {

            if (this.window === undefined) {
                this.window = wp.media({
                    title: 'Insert Particles',
                    library: {type: 'image'},
                    multiple: false,
                    button: {text: 'Insert'}
                });

                var self = this; // Needed to retrieve our variable in the anonymous function below
                this.window.on('select', function() {
                    var first = self.window.state().get('selection').first().toJSON();
                    console.log(first);
                    wp.media.editor.insert('[myshortcode id="' + first.id + '"]');
                });
            }

            this.window.open();
            return false;

        }


        //Add Image Uploder Button
        function media_upload(button_class) {

            var _custom_media = true,

                _orig_send_attachment = wp.media.editor.send.attachment;


            $('body').on('click', button_class, function (e) {

                var button_id = '#' + $(this).attr('id');

                var self = $(button_id);

                var send_attachment_bkp = wp.media.editor.send.attachment;

                var button = $(button_id);

                var id = button.attr('id').replace('_button', '');

                _custom_media = true;

                wp.media.editor.send.attachment = function (props, attachment) {

                    if (_custom_media) {

                        $('.custom_media_id').val(attachment.id);

                        $('.custom_media_url').val(attachment.url);

                        $('.custom_media_image').attr('src', attachment.url).css('display', 'block');

                    } else {

                        return _orig_send_attachment.apply(button_id, [props, attachment]);

                    }

                };

                wp.media.editor.open(button);

                return false;

            });

        }

        media_upload('.custom_media_button.button');

    });
});
